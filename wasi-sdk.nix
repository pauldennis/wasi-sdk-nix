{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv
, coreutils ? pkgs.coreutils
, git ? pkgs.git
, perl ? pkgs.perl
, bash ? pkgs.bash
, clang ? pkgs.clang
, cmake ? pkgs.cmake
, ninja ? pkgs.ninja
, python ? pkgs.python3

, dependencySnapshot

}:


stdenv.mkDerivation {

  name = "wasi-sdk";
  builder = ./builder.sh;

  buildInputs = [ coreutils git perl clang cmake ninja python ];

  inherit coreutils;
  inherit bash;

  inherit dependencySnapshot;

}
