set -e

echo "begin downloading"

rm -rf ./wasi-sdk-dependency-snapshot/ || true
rm -r ./wasi-sdk-dependency-snapshot.tar.gz || true

mkdir -p ./wasi-sdk-dependency-snapshot/

cd ./wasi-sdk-dependency-snapshot/
  git --version
  git config pack.threads 1

  git clone "https://github.com/TerrorJack/wasi-sdk.git" --single-branch
  cd wasi-sdk/
    echo "try to normalize"
    git repack -dnA
    git repack -A -d -f
    git gc --prune=now
    git gc --prune=all
    mv .git/objects .git/objects.tmp
    mkdir .git/objects
    git config core.compression 0
    find .git/objects.tmp/pack -name "*.pack" -exec sh -c "git unpack-objects < {}" \;
    rm -rf .git/config .git/objects.tmp

    git submodule init
    git submodule update --depth 90 --single-branch --recursive

    rm ../wasi-sdk/.git/index
  cd ..

  echo "delete submodule stuff"
  rm wasi-sdk/.gitmodules
  rm -rf wasi-sdk/.git/modules
  rm -rf wasi-sdk/src/config/.git
  rm -rf wasi-sdk/src/llvm-project/.git
  rm -rf wasi-sdk/src/wasi-libc/.git

  echo "delete non reproducible files"

  rm wasi-sdk/.git/hooks/*.sample || true
  rm -r wasi-sdk/.git/logs/* || true

  rm wasi-sdk/.git/modules/src/config/hooks/*.sample || true
  rm -r wasi-sdk/.git/modules/src/config/logs/* || true

  rm wasi-sdk/.git/modules/src/llvm-project/hooks/*.sample || true
  rm -r wasi-sdk/.git/modules/src/llvm-project/logs/* || true

  rm wasi-sdk/.git/modules/src/wasi-libc/hooks/*.sample || true
  rm -r wasi-sdk/.git/modules/src/wasi-libc/logs/* || true

  rm wasi-sdk/.git/modules/src/wasi-libc/modules/tools/wasi-headers/WASI/hooks/*.sample || true
  rm -r wasi-sdk/.git/modules/src/wasi-libc/modules/tools/wasi-headers/WASI/logs/* || true

cd ..

echo "put into archive"

tar --sort=name --owner=root:0 --group=root:0 --mtime='UTC 2000-01-01' -czf wasi-sdk-dependency-snapshot.tar.gz --directory=./wasi-sdk-dependency-snapshot/ .

echo "calculating hash..."
dependencyHash=$(nix-hash --type sha256 --flat --base32 wasi-sdk-dependency-snapshot.tar.gz)
echo "the hash is:"
echo $dependencyHash
ls ../fix/dependencySnapshotHash
echo $dependencyHash > ../fix/dependencySnapshotHash_new

if cmp -s ../fix/dependencySnapshotHash_new ../fix/dependencySnapshotHash; then
  echo "hashs are the same, hash did not change"
else
  echo "hash did change, the old hash is:"
  echo $(<../fix/dependencySnapshotHash)
fi


echo "move to packaging area"
mv ./wasi-sdk-dependency-snapshot.tar.gz ../internetSnapshotArtifact/

echo "finished downloading"
