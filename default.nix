import ./wasi-sdk.nix
{
  dependencySnapshot=builtins.fetchurl
  {
    url = "https://gitlab.com/pauldennis/wasi-sdk-nix/-/package_files/" + (import ./fix/gitlabFileNumber_String) + "/download";
    sha256 = import ./fix/dependencySnapshotHash_String; #TODO import string verbatime
  };
}
