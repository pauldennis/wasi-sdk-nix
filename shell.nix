{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

}:

pkgs.mkShell {
  buildInputs = [
    pkgs.git
    pkgs.curl
    pkgs.gnutar
    pkgs.cacert
    pkgs.nix
  ];

  shellHook = ''
    echo shell for wasi-sdk-nix
  '';

  MY_ENVIRONMENT_VARIABLE = "world";
}
