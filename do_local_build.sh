set +e

cd fix
  ./prepare.sh
cd ..

BEGINDATE=$(date --rfc-3339=s)

nix-build ./local.nix --show-trace

notify-send "done"

echo "begin"
echo $BEGINDATE
echo end
date --rfc-3339=s
