set -e

echo "install"
nix-env -iA nixpkgs.curl
nix-env -iA nixpkgs.jq

HASH_OF_PACKAGE=$(cat fix/dependencySnapshotHash_new)

PACKAGE_VERSION="${HASH_OF_PACKAGE}"
PACKAGE_NAME="wasi-sdk-nix-dependencies"
PACKAGE_FILE_GENERATED="${PACKAGE_NAME}.tar.gz"
PACKAGE_FILE_RELEASE="${PACKAGE_NAME}-${PACKAGE_VERSION}.tar.gz"
PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${PACKAGE_VERSION}"

echo "rename file to include version in name"
mv ./internetSnapshotArtifact/wasi-sdk-dependency-snapshot.tar.gz ./internetSnapshotArtifact/${PACKAGE_FILE_RELEASE}

doesPackageAlreadyExist=$(curl https://gitlab.com/api/v4/projects/25003732/packages | jq -r ".[] | select(.version==\"${PACKAGE_VERSION}\") | ._links | .web_path")

if [ -z "$doesPackageAlreadyExist" ]
then
  echo "package not upload yet"
  echo "upload package"
  curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file internetSnapshotArtifact/${PACKAGE_FILE_RELEASE} ${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_RELEASE}
  echo ""
else
  echo "package already exists"
fi

echo "now the package should exist:"
curl https://gitlab.com/api/v4/projects/25003732/packages | jq -r ".[] | select(.version==\"${PACKAGE_VERSION}\") | ._links | .web_path"
