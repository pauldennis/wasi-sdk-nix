# echo "########" && ls && echo "########"
echo "START###############################################"

# sets up all the convienience commands for building
source $stdenv/setup

STARTTIME=$(date --rfc-3339=s)
echo $STARTTIME

#TODO how is this supposed to be done?
mkdir -p /usr/bin/
ln -s $coreutils/bin/env /usr/bin/env || true
ln -s $bash/bin/bash /bin/bash || true

# The output is supposed to be installed into this location
# apparently the folder does not exist yet so we create it
mkdir $out

cd $out

echo "unpack dependencySnapshot"
tar xfz $dependencySnapshot

echo "setup repo"
cd wasi-sdk
echo "version"
echo "reset"
git reset
echo "status"
git status
./version.sh

echo "make package"
make package

echo "STOP################################################"

echo "begin time"
echo $STARTTIME
echo "end time"
date --rfc-3339=s
